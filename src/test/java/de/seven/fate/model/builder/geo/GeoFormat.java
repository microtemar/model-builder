package de.seven.fate.model.builder.geo;

/**
 * Created by Mario on 29.03.2016.
 */
public enum GeoFormat {
    LAMBERT, WGS64
}
